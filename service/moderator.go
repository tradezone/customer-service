package service

import (
	"context"

	pb "gitlab.com/tradezone/customer-service/genproto/customer"
	"gitlab.com/tradezone/customer-service/pkg/logger"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func (s *CustomerService) GetModerator(ctx context.Context, req *pb.GetModeratorReq) (*pb.GetModeratorRes, error) {
	moder, err := s.Storage.Customer().GetModerator(req)
	if err != nil {
		s.Logger.Error("Error while getting addmin by adminname", logger.Any("get", err))
		return &pb.GetModeratorRes{}, status.Error(codes.NotFound, "Your are not admin")
	}
	return moder, err
}
