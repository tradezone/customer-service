CURRENT_DIR=$(shell pwd)
APP=template
APP_CMD_DIR=./cmd

build:
	CGO_ENABLED=0 GOOS=darwin go build -mod=vendor -a -installsuffix cgo -o ${CURRENT_DIR}/bin/${APP} ${APP_CMD_DIR}/main.go

proto-gen:
	./scripts/gen-proto.sh	${CURRENT_DIR}

lint: ## Run golangci-lint with printing to stdout
	golangci-lint -c .golangci.yaml run --build-tags "musl" ./...

migrate_create:
	migrate create -ext sql -dir migrations -seq create_customers_table
	migrate create -ext sql -dir migrations -seq create_staff_table
	migrate create -ext sql -dir migrations -seq create_admins_table
	migrate create -ext sql -dir migrations -seq create_moderators_table

migrate_up:
	migrate -source file://migrations/ -database postgres://postgres:123@localhost:5432/customerdb up
migrate_down:
	migrate -source file://migrations/ -database postgres://postgres:123@localhost:5432/customerdb down

submod_up:
	git submodule update --remote --merge
create_submod:
	git submodule add git@gitlab.com:tradezone/protos.git
run:
	go run cmd/main.go