package postgres

import (
	"database/sql"
	"fmt"

	pb "gitlab.com/tradezone/customer-service/genproto/customer"
)

func (r *CustomerRepo) GetAdmin(req *pb.AdminCutomerReq) (*pb.AdminCustomerRes, error) {
	res := pb.AdminCustomerRes{}
	err := r.Db.QueryRow(`SELECT 
		id,
		admin_name,
		admin_password, 
		created_at, 
		updated_at
		FROM admins 
		WHERE deleted_at 
		IS NULL AND 
		admin_name=$1`, req.Name).
		Scan(&res.Id, &res.Name, &res.Password, &res.CreatedAt, &res.UpdatedAt)
	if err == sql.ErrNoRows {
		fmt.Println("No rows")
		return &res, nil
	}
	if err != nil {
		return &pb.AdminCustomerRes{}, err
	}
	return &res, nil
}
