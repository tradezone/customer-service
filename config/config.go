package config

import (
	"os"

	"github.com/spf13/cast"
)

type Config struct {
	Environment       string
	PostgresHost      string
	PostgresPort      int
	PostgresDatabase  string
	PostgresUser      string
	PostgresPassword  string
	LogLevel          string
	RPCPort           string
	RPCHost           string
	ReviewServiceHost string
	ReviewServicePort int

	PostServiceHost string
	PostServicePort int

	KafkaHost  string
	KafkaPort  string
	KafkaTopic string
}

func Load() Config {

	c := Config{}
	// mydatabase.cqwczror7wp5.us-east-1.rds.amazonaws.com
	c.Environment = cast.ToString(GetOrReturnDefault("ENVIRONMENT", "develop"))

	c.PostgresHost = cast.ToString(GetOrReturnDefault("POSTGRES_HOST", "localhost"))
	c.PostgresPort = cast.ToInt(GetOrReturnDefault("POSTGRES_PORT", 5432))
	c.PostgresDatabase = cast.ToString(GetOrReturnDefault("POSTGRES_DATABASE", "customerdb"))
	c.PostgresUser = cast.ToString(GetOrReturnDefault("POSTGRES_USER", "postgres"))
	c.PostgresPassword = cast.ToString(GetOrReturnDefault("POSTGRES_PASSWORD", "123"))

	c.PostServiceHost = cast.ToString(GetOrReturnDefault("POST_SERVICE_HOST", "localhost"))
	c.PostServicePort = cast.ToInt(GetOrReturnDefault("POST_SERVICE_PORT", 2222))

	c.ReviewServiceHost = cast.ToString(GetOrReturnDefault("REVIEW_SERVICE_HOST", "localhost"))
	c.ReviewServicePort = cast.ToInt(GetOrReturnDefault("REVIEW_SERVICE_PORT", 3333))

	c.KafkaHost = cast.ToString(GetOrReturnDefault("KAFKA_HOST", "kafka"))
	c.KafkaPort = cast.ToString(GetOrReturnDefault("KAFKA_PORT", "9092"))
	c.KafkaTopic = cast.ToString(GetOrReturnDefault("KAFKA_TOPIC", "postTopic"))

	c.LogLevel = cast.ToString(GetOrReturnDefault("LOG_LEVEL", "debug"))

	c.RPCPort = cast.ToString(GetOrReturnDefault("RPC_PORT", "1111"))
	c.RPCHost = cast.ToString(GetOrReturnDefault("RPC_HOST", "localhost"))
	return c
}

func GetOrReturnDefault(key string, defaultValue interface{}) interface{} {
	_, exists := os.LookupEnv(key)
	if exists {
		return os.Getenv(key)
	}
	return defaultValue
}
