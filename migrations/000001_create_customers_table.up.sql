CREATE TABLE IF NOT EXISTS customers(
    id serial primary key,
    first_name varchar(50),
    last_name varchar(50),
    email varchar(30) unique,
    user_name varchar(30) unique,
    phone_number varchar(35),
    role_type text,
    access_token text,
    created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    updated_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    deleted_at TIMESTAMP
);